'use strict'

const { Router } = require('express')
const router = Router()

const UserController = require('../controllers/users.controller.js')

router.get('/users', UserController.index)
router.get('/user/:id', UserController.show)
router.post('/users', UserController.store)
router.put('/user', UserController.update)
router.put('/user/pass',UserController.changePassword)

module.exports = router