'use strict'


const { Router } = require('express')
const ArchivoController = require('../controllers/archivos.controller.js')
const route = Router()

route.get('/archivos', ArchivoController.index)
route.get('/archivo/:id', ArchivoController.show)
route.post('/archivo', ArchivoController.store)
route.put('/archivo', ArchivoController.update)

module.exports = route