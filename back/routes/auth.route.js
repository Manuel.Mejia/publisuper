'use strict'

const { Router } = require('express')
const router = Router()

const AuthController = require('../controllers/auth.controller.js')

router.post('/auth', AuthController.auth)

module.exports = router