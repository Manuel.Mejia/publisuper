'use strict'

const { Router } = require('express')
const route = Router()

const TerceroController = require('../controllers/terceros.controller.js')

route.get('/terceros/:option', TerceroController.index)
route.get('/tercero/:option/:id',TerceroController.show)
route.post('/terceros/:option',TerceroController.store)
route.put('/tercero/:option',TerceroController.update)


module.exports = route