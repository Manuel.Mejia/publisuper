'use strict'

const conn = require('../config/db.js')

class AuthRepository {
	async login(user) {
		const columns = [ 'contrasenna', 'rol' ]
		return await conn`SELECT ${ conn(columns) } FROM usuarios WHERE usuario = ${ user }`
	}
}

module.exports = new AuthRepository