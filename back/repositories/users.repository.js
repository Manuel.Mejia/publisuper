'use strict'

const conn = require('../config/db.js')

class UserRepository {
	async all() {
		const columns = ['id', 'usuario', 'rol', 'estado']
		return await conn`SELECT ${ conn(columns) } FROM usuarios`
	}

	async find(id) {
		const columns = ['id', 'usuario', 'rol', 'estado']
		return await conn`SELECT ${ conn(columns) } FROM usuarios WHERE id = ${ id }`
	}

	async create(user) {
		return await conn`INSERT INTO usuarios ${ conn(user, 'usuario', 'contrasenna', 'rol', 'estado') } RETURNING usuario, rol, estado`
	}

	async update(user) {
		return await conn`UPDATE usuarios SET ${ conn(user, 'usuario', 'rol', 'estado') } WHERE id = ${ user.id } RETURNING id, usuario, rol, estado`
	}

	async changePass(user){
		return await conn`UPDATE usuarios SET ${ conn(user, 'contrasenna')} WHERE id = ${ user.id} RETURNING id, usuario, rol, estado`
	}
}

module.exports = new UserRepository()