'use strict'

const conn = require('../config/db.js')

class ArchivoRepository {
	async all(){
		let columns = ['id', 'codigo', 'archivo_empresa', 'estado', 'tipo_empresa']
		return await conn`SELECT ${conn(columns)} FROM archivos`
	}

	async find(id){
		let columns = ['id', 'codigo', 'archivo_empresa', 'estado', 'tipo_empresa']
		return await conn`SELECT ${conn(columns)} FROM archivos WHERE id = ${id}`
	}

	async create(model){
		return await conn`INSERT INTO archivos ${ conn(model, 'codigo', 'archivo_empresa', 'estado', 'tipo_empresa') }
							RETURNING codigo, archivo_empresa, tipo_empresa`
	}

	async update(model){
		return await conn`UPDATE archivos SET ${ conn(model, 'archivo_empresa', 'estado', 'tipo_empresa')}
							WHERE id = ${ model.id } 
							RETURNING codigo, archivo_empresa, estado `
	}
}

module.exports = new ArchivoRepository