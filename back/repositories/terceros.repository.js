'use strict'

const conn = require('../config/db.js')

class TerceroRepository{
	async all(option){
		
		if(option == 'empresas'){
			return await conn`SELECT a.id, a.nit, a.verificacion, a.razon_social, a.ciudad, a.direccion, a.fijo, a.celular, a.email, a.tipo_empresa, a.tercero, a.usuario,
							 b.codigo, b.tercero FROM empresas AS a INNER JOIN terceros AS b ON a.tercero = b.id`

		}else if(option == 'personas'){
			return await conn`SELECT a.id, a.cedula, a.nombre, a.apellido, a.direccion, a.ciudad, a.telefono, a.celular, a.email, a.tercero, a.usuario ,
								 b.id, b.codigo, b.tercero FROM personas AS a INNER JOIN terceros AS b ON a.tercero = b.id`
		}
		
	}

	async find(tipo, id){
		if(tipo == 'empresas'){
			return await conn`SELECT a.nit, a.verificacion, a.razon_social, a.ciudad, a.direccion, a.fijo, a.celular, a.email, b.codigo, b.tercero FROM empresas AS a 
								INNER JOIN terceros AS b ON a.tercero = b.id
								WHERE a.id = ${id}`

		}else if(tipo == 'personas'){
			return await conn`SELECT a.cedula, a.nombre, a.apellido, a.direccion, a.ciudad, a.telefono, a.celular, a.email, b.codigo, b.tercero 
								FROM personas AS a INNER JOIN terceros AS b ON a.tercero = b.id
								WHERE a.id = ${id}`
		}
	}

	async create(tipo, model){
		if(tipo == 'personas'){

			return await conn`INSERT INTO personas ${ conn(model, 'cedula', 'nombre', 'apellido', 'direccion', 'ciudad', 'telefono', 'celular', 'email', 'tercero', 'usuario')}
								RETURNING cedula, nombre, apellido`

		}else if(tipo == 'empresas'){
			
			return await conn`INSERT INTO empresas ${ conn(model, 'nit', 'verificacion', 'razon_social', 'ciudad', 'direccion', 'fijo', 'celular', 'email', 'tipo_empresa', 'tercero', 'usuario' )}
								RETURNING nit, razon_social, email`
		}
	}

	async update(tipo, model){
		if(tipo == 'personas'){
			return await conn`UPDATE personas SET ${ conn(model,'nombre', 'apellido', 'direccion', 'ciudad', 'telefono', 'celular', 'email')} 
								WHERE id = ${model.id} RETURNING cedula, nombre, email`

		}else if(tipo == 'empresas'){
			return await conn`UPDATE empresas SET ${ conn(model,'razon_social', 'ciudad', 'direccion', 'fijo', 'celular', 'email', 'tipo_empresa' )}
								WHERE id = ${model.id} RETURNING nit, razon_social, email`
		}

	}
}

module.exports = new TerceroRepository