'use strict'

class TerceroModel {
	constructor() {
		this.id = null
		this.codigo = null
		this.tercero = null
		this.estado = null
	}
}

module.exports = new TerceroModel()