'use strict'

class EmpresasModel{
	constructor(){
		this.id = null
		this.nit = null
		this.verificacion = null
		this.razon_social = null
		this.ciudad = null
		this.direccion = null
		this.fijo = null
		this.celular = null
		this.email = null
		this.tipo_empresa = null
		this.tercero = null
		this.usuario = null
	}
}

module.exports = new EmpresasModel()