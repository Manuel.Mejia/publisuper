'use strict'

class UserModel {
	constructor() {
		this.id = null
		this.usuario = null
		this.contrasenna = null
		this.rol = null
		this.estado = null
	}
}

module.exports = new UserModel()