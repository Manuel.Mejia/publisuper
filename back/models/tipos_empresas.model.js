'use strict'

class TiposEmpresasModel{
	constructor(){
		this.id = null
		this.codigo = null
		this.tipo = null
	}
}

module.exports = new TiposEmpresasModel()