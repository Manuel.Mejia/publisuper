'use strict'

class ArchivosModel{
	constructor(){
		this.id = null
		this.codigo = null
		this.archivo_empresa = null
		this.estado = null
		this.tipo_empresa = null
	}
}

module.exports = new ArchivosModel()