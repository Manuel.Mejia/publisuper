'use strict'

class PersonasModel{
	constructor(){
		this.id = null
		this.cedula = null
		this.nombre = null
		this.apellido = null
		this.direccion = null
		this.ciudad = null
		this.telefono = null
		this.celular = null
		this.email = null
		this.tercero = null
		this.usuario = null
	}
}

module.exports = new PersonasModel()