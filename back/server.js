'use strict'

const log4js = require("log4js")
const app = require('./index.js')

const logger = log4js.getLogger()
logger.level = "debug"

app.listen({ 'host': app.get('host'), 'port': app.get('port') }, () => {
	logger.debug(`Server -> ${ app.get('host') }:${ app.get('port') }`)
})