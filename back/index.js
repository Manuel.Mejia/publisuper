'use strict'

// Libreries
const express = require('express')
const log4js = require("log4js")
const dotenv = require('dotenv')
const cors = require('cors')
const path = require('path')

const Middlewares = require('./Middlewares.js')

const app = express()

// configs
const logger = log4js.getLogger()
logger.level = "debug"

dotenv.config()
app.set('host', process.env.HOST || 'localhost')
app.set('port', process.env.PORT || 5000)
app.use(express.json())

app.use(cors())

// Middlewares
app.use(Middlewares.headers)
app.use(Middlewares.timeLog)

// Routes
app.use('/api', require(path.join(__dirname, 'routes', 'auth.route.js')))
app.use('/api', Middlewares.guard, require(path.join(__dirname, 'routes', 'users.route.js')))
app.use('/api', Middlewares.guard, require(path.join(__dirname, 'routes', 'terceros.route.js')))
app.use('/api', Middlewares.guard, require(path.join(__dirname, 'routes', 'archivos.route.js')))

module.exports = app