'use struct'

const log4js = require("log4js")
const jwt = require('jsonwebtoken')
const config = require('./config/config.js')

const logger = log4js.getLogger()
logger.level = "debug"

class Middlewares {
	guard(req, res, next) {
		const token = req.headers['access-token']

		if(token) {
			jwt.verify(token, config.key, (err, decoded) => {
				if(err) { return res.json({ mensaje: 'Token inválido' }) }
				req.decoded = decoded
				next()
			})
		} else {
			res.send({ mensaje: 'Token no proveído.' })
		}
	}

	headers(req, res, next) {
		res.header("Access-Control-Allow-Origin","*")
		res.header("Access-Control-Allow-Methods","GET,PUT,POST")
		res.header("Access-Control-Allow-Headers","Content-type,Accept")
	
		res.contentType('application/json')
		next()
	}

	timeLog(req, res, next) {
		const date = new Date().toLocaleString('es-CO', {
			timeZone: 'America/Bogota'
		})
		logger.debug(`${req.method} ${req.path} -> ${date}`)
		next();
	}
}

module.exports = new Middlewares()