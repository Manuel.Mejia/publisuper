-- CREATE USER publisuper PASSWORD 'publi1234!';
-- ALTER ROLE publisuper WITH SUPERUSER;
-- CREATE DATABASE publisuper WITH OWNER publisuper;
-- GRANT ALL PRIVILEGES ON DATABASE publisuper TO publisuper;

DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TYPE STATUS AS ENUM('ACT','INCT', 'WAIT');
CREATE TYPE ROLES AS ENUM('cliente', 'admin');
CREATE TYPE SECTOR AS ENUM('cooperativa', 'transporte', 'natural');

CREATE TABLE tipos_empresas(
	id SERIAL PRIMARY KEY,
	codigo VARCHAR NOT NULL,
	tipo SECTOR NOT NULL,	
	UNIQUE(codigo)
);

CREATE TABLE archivos(
	id SERIAL PRIMARY KEY,
	codigo VARCHAR NOT NULL,
	archivo_empresa TEXT NOT NULL,
	estado STATUS NOT NULL,
	tipo_empresa INT NOT NULL,
	FOREIGN KEY (tipo_empresa) REFERENCES tipos_empresas(id),
	UNIQUE(codigo)
);

CREATE TABLE terceros(
	id 	SERIAL PRIMARY KEY,
	codigo VARCHAR NOT NULL,
	tercero VARCHAR(50) NOT NULL,
	estado STATUS NOT NULL,
	UNIQUE(codigo)
);

CREATE TABLE usuarios(
	id SERIAL PRIMARY KEY NOT NULL,
	usuario VARCHAR(100) NOT NULL,
	contrasenna TEXT NOT NULL,
	rol ROLES NOT NULL DEFAULT('cliente'),
	estado STATUS NOT NULL,
	UNIQUE(usuario)
);

CREATE TABLE empresas(
	id SERIAL NOT NULL,
	nit INT NOT NULL,
	verificacion INT NOT NULL,
	razon_social VARCHAR(100) NOT NULL,
	ciudad VARCHAR(100) NOT NULL,
	direccion VARCHAR(100) NOT NULL,
	fijo VARCHAR(20) NOT NULL,
	celular VARCHAR(20) NOT NULL,
	email VARCHAR(100) NOT NULL,	
	tipo_empresa INT,
	tercero INT NOT NULL,
	usuario INT NOT NULL,
	FOREIGN KEY(tipo_empresa) REFERENCES tipos_empresas(id),
	FOREIGN KEY(tercero) REFERENCES terceros(id),
	FOREIGN KEY(usuario) REFERENCES usuarios(id),
	UNIQUE(nit, verificacion),
	UNIQUE(email)
);

CREATE TABLE personas(
	id SERIAL NOT NULL,
	cedula INT NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	apellido VARCHAR(100) NOT NULL,	
	direccion VARCHAR(100) NOT NULL,
	ciudad VARCHAR(100) NOT NULL,
	telefono VARCHAR(20) NOT NULL,
	celular VARCHAR(20) NOT NULL,
	email VARCHAR(100) NOT NULL,
	tercero INT NOT NULL,
	usuario INT NOT NULL,
	FOREIGN KEY(tercero) REFERENCES terceros(id),
	FOREIGN KEY(usuario) REFERENCES usuarios(id),
	UNIQUE(cedula),
	UNIQUE(email)
);




