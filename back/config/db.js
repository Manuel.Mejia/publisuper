'use strict'
//conexion a base de datos
const postgres = require('postgres')

const conn = postgres({
	host: process.env.HOTS,
	port: process.env.POSTGRES_PORT,
	database: process.env.POSTGRES_DB,
	username: process.env.POSTGRES_USERNAME,
	password: process.env.POSTGRES_PASSWORD
})

module.exports = conn
