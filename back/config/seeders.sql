DELETE FROM archivos;
DELETE FROM empresas;
DELETE FROM tipos_empresas;
DELETE FROM personas;
DELETE FROM usuarios;
DELETE FROM terceros;

ALTER SEQUENCE archivos_id_seq RESTART WITH 1;
ALTER SEQUENCE tipos_empresas_id_seq RESTART WITH 1;
ALTER SEQUENCE terceros_id_seq RESTART WITH 1;
ALTER SEQUENCE empresas_id_seq RESTART WITH 1;
ALTER SEQUENCE personas_id_seq RESTART WITH 1;
ALTER SEQUENCE usuarios_id_seq RESTART WITH 1;

INSERT INTO tipos_empresas(codigo,tipo)
	VALUES
		('EMP_01','cooperativa'),
		('EMP_02', 'transporte');

INSERT INTO archivos(codigo,archivo_empresa, estado, tipo_empresa)
	VALUES
		( 'COO_01', 'cooperativa.zip','ACT',1),
		('TRANS_01','tranporte.zip','ACT',2);

INSERT INTO terceros(codigo, tercero, estado)
	VALUES
		('T_01', 'empresa', 'ACT'),
		('T_02', 'empresa', 'ACT');

INSERT INTO usuarios(usuario, contrasenna, rol, estado)
	VALUES('felipe', '$2b$10$PMSVK2eUsyeRnmCVfkpvje8WLcSmjV906dO0eU8cC.mJj3rgWdi4y', 'cliente', 'ACT'),
			('juanita', '$2b$10$fUFpGA7eStgAGX1H3MvdnO5XwhYQxEiY0K0n.2/MsMROX6EN3llwW', 'cliente', 'ACT'),
			('pepito', '$2b$10$fUFpGA7eStgAGX1H3MvdnO5XwhYQxEiY0K0n.2/MsMROX6EN3llwW', 'cliente', 'ACT');

INSERT INTO empresas(nit,verificacion , razon_social,ciudad,direccion, fijo, celular, email, tipo_empresa, tercero, usuario)
	VALUES
		(147856556, 8, 'felipe SAS', 'BOGOTA','cr 55','789789','2334242234','empresa1@email.com', 1, 1, 1),
		(243245434, 6, 'daniel SAS', 'pereira','cr 53','789856','2334435234','empresa2@email.com', 2 ,2, 2);

INSERT INTO personas(id, cedula, nombre, apellido, direccion, ciudad, telefono, celular, email, tercero, usuario)
	VALUES(default, 1478526,'felipe','sanabria', 'cr 55', 'BOGOTA','8799845', '3215458' ,'felipe@email.com', 2, 3);


