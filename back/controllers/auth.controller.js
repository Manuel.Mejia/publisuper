'use strict'

const jwt = require('jsonwebtoken')
const config = require('../config/config.js')
const bcrypt = require('bcrypt')
const AuthRepository = require('../repositories/auth.repository.js')

class AuthController {
	async auth(req, res) {
		let response = { token: null }
		const login = await AuthRepository.login(req.body.usuario)

		if(login.count > 0) {
			const hash = login[0].contrasenna
			bcrypt.compare(req.body.contrasenna, hash, function(err, result) {
				if(result) {
					response.token = jwt.sign({ usr: req.body.usuario, rol: login[0].rol }, config.key, { expiresIn: "1h" })
				}
				res.send(response)
			})
		} else {
			res.send(response)
		}
	}
}

module.exports = new AuthController()