'use strict'

const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const salt = 10

const UsersModel = require('../models/users.model.js')
const UserRepository = require('../repositories/users.repository.js')
const AuthRepository = require('../repositories/auth.repository.js')

class UserController {
	async index(req, res) {
		res.json({ payload: await UserRepository.all() })
	}

	async show(req, res) {
		const id = req.params.id
		res.json({ payload: await UserRepository.find(id) })
	}

	async store(req, res) {
		bcrypt.hash(req.body.contrasenna, salt, async (err, hash) => {
			if(!err) {
				UsersModel.usuario = req.body.usuario
				UsersModel.contrasenna = hash
				UsersModel.rol = req.body.rol
				UsersModel.estado = req.body.estado
	
				res.json({ payload: await UserRepository.create(UsersModel) })
			}
			res.send({ mensaje: 'Hash Error' })
		})
	}

	async update(req, res) {
		UsersModel.id = req.body.id
		UsersModel.usuario = req.body.usuario
		UsersModel.rol = req.body.rol
		UsersModel.estado = req.body.estado

		res.json({ payload: await UserRepository.update(UsersModel) })
	}


	async changePassword(req,res){
		const token = req.headers['access-token']
		let usuario = jwt.decode(token)

		if(usuario.rol == 'cliente'){
			res.json({payload: 'No permitido'})

		}else if(usuario.rol == 'admin'){
			let usuario = await AuthRepository.login(req.body.usuario)
			
			if(usuario.count > 0){
				let hash = usuario[0].contrasenna
				bcrypt.compare(req.body.contrasenna, hash, async (err,result) => {
					if(result){
						bcrypt.hash(req.body.new_contrasenna, salt, async (err, result) => {
							if(!err){
								UsersModel.id = req.body.id
								UsersModel.usuario = req.body.usuario
								UsersModel.contrasenna = result

								res.json({ payload: await UserRepository.changePass(UsersModel) })
							}
						})						
					}
				})

			}else{
				res.send('usuario no existe')
			}
			
		}		
	}
}

module.exports = new UserController()