'use strict'

const ArchivoRepository = require('../repositories/archivos.repository.js')
const ArchivoModel = require('../models/archivos.model.js')

class ArchivoController{
	async index(req, res){
		res.json({ payload: await ArchivoRepository.all()})
	}

	async show(req,res){
		let id = req.params.id

		res.json({ payload: await ArchivoRepository.find(id) })
	}

	async store(req,res){

		ArchivoModel.codigo = req.body.codigo
		ArchivoModel.archivo_empresa = req.body.archivo_empresa
		ArchivoModel.estado = req.body.estado
		ArchivoModel.tipo_empresa = req.body.tipo_empresa

		res.json({ payload: await ArchivoRepository.create(ArchivoModel)})
	}

	async update(req,res){

		ArchivoModel.id = req.body.id
		ArchivoModel.codigo = req.body.codigo
		ArchivoModel.archivo_empresa = req.body.archivo_empresa
		ArchivoModel.estado = req.body.estado
		ArchivoModel.tipo_empresa = req.body.tipo_empresa

		res.json({ payload: await ArchivoRepository.update(ArchivoModel) })
	}

}

module.exports = new ArchivoController