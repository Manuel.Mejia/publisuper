'use strict'

const TerceroRepository = require('../repositories/terceros.repository.js')
const PersonModel = require('../models/personas.model.js')
const EnterpriseModel = require('../models/empresas.model.js')


class TerceroController{
	async index(req,res){
		const option = req.params.option
		res.json({ payload: await TerceroRepository.all(option) })

	}

	async show(req,res){
		let tipo = req.params.option
		let id = req.params.id
		res.json({ payload: await TerceroRepository.find(tipo,id)})
	}

	async store(req,res){

		let tipo = req.params.option		

		if(tipo == 'personas'){
			PersonModel.cedula = req.body.cedula
			PersonModel.nombre = req.body.nombre
			PersonModel.apellido = req.body.apellido
			PersonModel.direccion = req.body.direccion
			PersonModel.ciudad = req.body.ciudad
			PersonModel.telefono = req.body.telefono
			PersonModel.celular = req.body.celular
			PersonModel.email = req.body.email
			PersonModel.tercero = req.body.tercero
			PersonModel.usuario = req.body.usuario

			res.json({ payload: await TerceroRepository.create(tipo, PersonModel)})
			
		}else if(tipo == 'empresas'){

			EnterpriseModel.nit = req.body.nit
			EnterpriseModel.verificacion = req.body.verificacion
			EnterpriseModel.razon_social = req.body.razon_social
			EnterpriseModel.ciudad = req.body.ciudad
			EnterpriseModel.direccion = req.body.direccion
			EnterpriseModel.fijo = req.body.fijo
			EnterpriseModel.celular = req.body.celular
			EnterpriseModel.email = req.body.email
			EnterpriseModel.tipo_empresa = req.body.tipo_empresa
			EnterpriseModel.tercero = req.body.tercero
			EnterpriseModel.usuario = req.body.usuario

			res.json({ payload: await TerceroRepository.create(tipo, EnterpriseModel)})
		}	

		res.send({message: 'error'})		
	}

	async update(req,res){

		let tipo = req.params.option
		
		if(tipo == 'personas'){

			PersonModel.id = req.body.id
			PersonModel.cedula = req.body.cedula
			PersonModel.nombre = req.body.nombre
			PersonModel.apellido = req.body.apellido
			PersonModel.direccion = req.body.direccion
			PersonModel.ciudad = req.body.ciudad
			PersonModel.telefono = req.body.telefono
			PersonModel.celular = req.body.celular
			PersonModel.email = req.body.email
			PersonModel.tercero = req.body.tercero		
			PersonModel.usuario = req.body.usuario			

			res.json({ payload: await TerceroRepository.update(tipo, PersonModel)})
			
		}else if(tipo == 'empresas'){

			EnterpriseModel.id = req.body.id
			EnterpriseModel.nit = req.body.nit
			EnterpriseModel.verificacion = req.body.verificacion
			EnterpriseModel.razon_social = req.body.razon_social
			EnterpriseModel.ciudad = req.body.ciudad
			EnterpriseModel.direccion = req.body.direccion
			EnterpriseModel.fijo = req.body.fijo
			EnterpriseModel.celular = req.body.celular
			EnterpriseModel.email = req.body.email
			EnterpriseModel.tipo_empresa = req.body.tipo_empresa
			EnterpriseModel.tercero  =req.body.tercero
			EnterpriseModel.usuario = req.body.usuario

			res.json({ payload: await TerceroRepository.update(tipo, EnterpriseModel)})
		}				
	}
}

module.exports = new TerceroController